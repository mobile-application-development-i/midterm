import 'package:flutter/material.dart';

class Home extends StatefulWidget {

  const Home({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {

  static const TextStyle headerStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  static const TextStyle titleStyle =
  TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

  static const TextStyle contentStyle =
  TextStyle(fontSize: 16);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return [
          SliverAppBar(
            title: Container(
                child:Text('ประกาศเรื่อง',
                style: headerStyle,)
            ),
            elevation: 10.0,
            automaticallyImplyLeading: false,
            expandedHeight:50,
            floating: true,
            snap: true,
            backgroundColor: Colors.blueGrey[100],
          )
        ];
      },
        body: SingleChildScrollView(
            child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(5.0),
                      padding: const EdgeInsets.all(16.0),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(20)
                        ),
                      ),
                      child: Column(
                        children: <Widget>[
                          Text(' 1.การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565(ด่วน)',
                              style: titleStyle),
                          Image(
                            image: AssetImage("assets/images/grad65.png"),
                          ),
                          Text(
                              '**นิสิตต้องยื่นคำร้องขอสำเร็จการศึกษาและชำระเงินตามกำหนดเวลา หากนิสิตไม่ได้ยื่นสำเร็จการศึกษาทางกองทะบียนฯ จะไม่เสนอชื่อสำเร็จการศึกษา คู่มือการยื่นคำร้องสำเร็จการศึกษา นิสิตพิมพ์ใบชำระเงินที่เมนูแจ้งจบและขึ้นทะเบียนนิสิต',
                          style: contentStyle,),
                        ],
                      ),
                    ),

                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(5.0),
                          padding: const EdgeInsets.all(16.0),
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.grey),
                            borderRadius: BorderRadius.all(Radius.circular(20)
                            ),
                          ),
                          child: Column(
                            children: <Widget>[
                              Text(' 2.กำหนดการชำระค่าธรรมเนียมการลงทะเบียนเรียนภาคปลาย ปีการศึกษา 2565',
                                  style: titleStyle),
                              Image(
                                image: AssetImage("assets/images/pay65.jpg"),
                              ),
                              Text(
                                  'สำหรับการ scan ชำระค่าธรรมเนียมการศึกษา แอปธนาคารกสิกรไทย ไม่สามารถชำระได้ ให้ใช้แอปของธนาคารอื่นๆ ขออภัยในความไม่สะดวกครับ',
                              style: contentStyle,),
                            ],
                          ),
                        ),
                      ],
                    ),

                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(5.0),
                          padding: const EdgeInsets.all(16.0),
                          decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.grey),
                            borderRadius: BorderRadius.all(Radius.circular(20)
                            ),
                          ),
                          child: Column(
                            children: <Widget>[
                              Text(' 3.กำหนดยื่นคำร้องเกี่ยวกับงานพิธีพระราชทานปริญญาบัตรปีการศึกษา 2563 และปีการศึกษา 2564',
                              style: titleStyle,),
                              Image(
                                image: AssetImage("assets/images/graduatePic.jpg"),
                              ),
                              Text('1.นิสิตที่ประสงค์ขอเปลี่ยนคำนำหน้าคำอ่านเพื่อแต่งชุดปกติขาวตามชั้นยศในวันพิธี'
                                  'ให้นิสิตยื่นความประสงค์ที่ลิงค์ คลิกที่นี่\n'
                                  '2. นิสิตที่ประสงค์ขอรับเป็นภิกษุในวันพิธี ให้นิสิตยื่นความประสงค์ที่ลิงค์ คลิกที่นี่\n'
                                  ' 3. นิสิตที่ประสงค์รับเป็นบัณฑิตพิเศษ ให้นิสิตยื่นความประสงค์ที่ลิงค์ คลิกที่นี่\n'
                                  '4. นิสิตที่ประสงค์ขอแต่งกายตามเพศที่แสดงออกในวันพิธี\n'
                                  '(เฉพาะเพศชายแต่งเป็นหญิง หรือ เพศหญิงแต่งเป็นชาย) ให้นิสิตยื่นความประสงค์ที่ลิงค์ คลิกที่นี\n่'
                                  '*** ผู้ที่สำเร็จการศึกษาปี 2563 ยื่นเอกสารไม่เกิน วันที่ 3 ก.พ. 2566 *(ขยายเวลา)\n'
                                  '*** ผู้ที่สำเร็จการศึกษาปี 2564 ยื่นเอกสารไม่เกิน วันที่ 31 ก.ค. 2566**\n',
                              style: contentStyle,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}