import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:midterm_reg/login.dart';

class Others extends StatefulWidget {

  const Others({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _OthersState();
  }
}

class _OthersState extends State<Others> {

  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            child: Column(
              children: <Widget>[
                Card(
                  margin: EdgeInsets.all(25.0),
                  shadowColor: Colors.grey[800],
                  child: Column(
                    children: <Widget>[
                      ListTile(
                        leading: CircleAvatar(
                          backgroundImage: AssetImage("assets/images/studentimage.jpg"),
                          radius: 30,
                         ),
                        title: Text(
                          'Yada Sukawipat',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 25),
                        ),
                        subtitle: Text(
                          '63160069',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(35, 10, 0, 10),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'เมนูอื่นๆ',
                    style: optionStyle,
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(50, 10, 0, 10),
                  child: othersItems(),
                ),
                Divider(
                  color: Colors.grey,
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(50, 10, 0, 10),
                  child: logOutButton(),
                ),
                Divider(
                  color: Colors.grey,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget othersItems() {
  return Container(
      child: Column(
      children: <Widget>[
        registerButton(),
        resultRegButton(),
        graduateButton(),
        paymentsButton(),
        calendarButton(),
        roomButton(),
        studentInfoButton(),
      ],
    ),
  );
}

Widget registerButton() {
  const TextStyle buttonStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Colors.grey);

  return Row(
    children: <Widget>[
      Icon(
        Icons.app_registration_outlined,
        color: Colors.grey,
        size: 24.0,
      ),
      TextButton(
        child: Text(
          'ลงทะเบียน',
          style: buttonStyle,
      ),
      onPressed: () {
          Get.dialog(alertDialog());
      },
      ),
    ],
  );
}

Widget resultRegButton() {
  const TextStyle buttonStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Colors.grey);

  return Row(
    children: <Widget>[
      Icon(
        Icons.fact_check_outlined,
        color: Colors.grey,
        size: 24.0,
      ),
      TextButton(
        child: Text(
          'ผลการลงทะเบียน',
          style: buttonStyle,
        ),
        onPressed: () {},
      ),
    ],
  );
}

Widget graduateButton() {
  const TextStyle buttonStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Colors.grey);

  return Row(
    children: <Widget>[
      Icon(
        Icons.school,
        color: Colors.grey,
        size: 24.0,
      ),
      TextButton(
        child: Text(
          'ตรวจสอบวันสำเร็จการศึกษา',
          style: buttonStyle,
        ),
        onPressed: () {},
      ),
    ],
  );
}

Widget paymentsButton() {
  const TextStyle buttonStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Colors.grey);

  return Row(
    children: <Widget>[
      Icon(
        Icons.payments,
        color: Colors.grey,
        size: 24.0,
      ),
      TextButton(
        child: Text(
          'ภาระค่าใช้จ่าย',
          style: buttonStyle,
        ),
        onPressed: () {},
      ),
    ],
  );
}

Widget roomButton() {
  const TextStyle buttonStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Colors.grey);

  return Row(
    children: <Widget>[
      Icon(
        Icons.meeting_room_rounded,
        color: Colors.grey,
        size: 24.0,
      ),
      TextButton(
        child: Text(
          'ตารางการใช้ห้อง',
          style: buttonStyle,
        ),
        onPressed: () {},
      ),
    ],
  );
}

Widget calendarButton() {
  const TextStyle buttonStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Colors.grey);

  return Row(
    children: <Widget>[
      Icon(
        Icons.calendar_month_rounded,
        color: Colors.grey,
        size: 24.0,
      ),
      TextButton(
        child: Text(
          'ปฏิทินการศึกษา',
          style: buttonStyle,
        ),
        onPressed: () {},
      ),
    ],
  );
}

Widget studentInfoButton() {
  const TextStyle buttonStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Colors.grey);

  return Row(
    children: <Widget>[
      Icon(
        Icons.app_registration_outlined,
        color: Colors.grey,
        size: 24.0,
      ),
      TextButton(
        child: Text(
          'ประวัตินิสิต',
          style: buttonStyle,
        ),
        onPressed: () {},
      ),
    ],
  );
}

Widget logOutButton() {
  const TextStyle buttonStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: Colors.red);

  return Row(
    children: <Widget>[
      TextButton.icon(
        icon: Icon(Icons.logout_outlined),
        label: Text('ออกจากระบบ',
        style: buttonStyle,),
        onPressed: () {
          Get.to(Login());
        },
        style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.red),
        ),
      ),
    ],
  );
}

Widget alertDialog() {
  return AlertDialog(
    title: Text("แจ้งเตือน"),
    titleTextStyle:
    TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 20),
    actionsOverflowButtonSpacing: 20,
    actions: [
      ElevatedButton(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.grey.shade100,),),
          onPressed: (){
        Get.back();
       }, child: Text("ปิด")),
    ],
    content: Text("ไม่อยู่ในช่วงการลงทะเบียน"),
  );
}