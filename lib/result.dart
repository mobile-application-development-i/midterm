import 'package:flutter/material.dart';

class Result extends StatefulWidget {

  const Result({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ResultState();
  }
}

List<String> yearList = <String>['2563', '2564', '2565', '2566'];

class _ResultState extends State<Result> {

  static const TextStyle optionStyle =
  TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  static const TextStyle titleStyle =
  TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

  static const TextStyle concludeStyle =
  TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.yellow);

  String dropdownYear = yearList.elementAt(2);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 1),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                          children: <Widget>[
                            Text(
                              'ผลการศึกษา',
                              style: optionStyle,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('ปีการศึกษา     ',
                                  style: titleStyle,
                                ),
                                dropDownYear(),
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.all(10.0),
                              height: 100,
                              width: 200,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.yellow.shade50,
                              ),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    'เกรดปัจจุบัน',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.yellow.shade600
                                    ),
                                  ),
                                  Text('3.48',
                                    style: TextStyle(
                                        fontSize: 50,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.yellow.shade400
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
              
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(10.0),
                child: Text('ภาคที่ 1',
                style: titleStyle,),
              ),
              Card(
                margin: EdgeInsets.all(10.0),
                shadowColor: Colors.grey[800],
                child: Container(
                  child: Table(
                    defaultColumnWidth: FlexColumnWidth(20.0),
                    children: [
                      TableRow( children: [
                        Column(children:[Text('รหัสวิชา', style: titleStyle)]),
                        Column(children:[Text('วิชา', style: titleStyle)]),
                        Column(children:[Text('หน่วยกิต', style: titleStyle)]),
                        Column(children:[Text('ระดับ', style: titleStyle)]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88634159')]),
                        Column(children:[Text('การพัฒนาซอฟต์แวร์')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('B')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88624259')]),
                        Column(children:[Text('หลักการโปรแกรมบนอุปกรณ์เคลื่อนที่')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('B')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88633159')]),
                        Column(children:[Text('เครือข่ายคอมพิวเตอร์')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('C+')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88635359')]),
                        Column(children:[Text('การพัฒนาและการออกแบบส่วนติดต่อผู้ใช้งาน')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('B+')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88631159')]),
                        Column(children:[Text('การออกแบบขั้นตอนวิธีและการประยุกต์')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('D+')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88636159')]),
                        Column(children:[Text('ปัญญาประดิษฐ์เบื้องต้น')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('A')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('23529164')]),
                        Column(children:[Text('ภาษาจีนกับวัฒนธรรมการสื่อสาร')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('A')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('รวม',style: concludeStyle)]),
                        Column(children:[Text('')]),
                        Column(children:[Text('21',style: concludeStyle)]),
                        Column(children:[Text('3.07',style: concludeStyle)]),
                      ]),
                    ],
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(10.0),
                child: Text('ภาคที่ 2',
                  style: titleStyle,),
              ),
              Card(
                margin: EdgeInsets.all(10.0),
                shadowColor: Colors.grey[800],
                child: Container(
                  child: Table(
                    defaultColumnWidth: FlexColumnWidth(20.0),
                    children: [
                      TableRow( children: [
                        Column(children:[Text('รหัสวิชา', style: titleStyle)]),
                        Column(children:[Text('วิชา', style: titleStyle)]),
                        Column(children:[Text('หน่วยกิต', style: titleStyle)]),
                        Column(children:[Text('ระดับ', style: titleStyle)]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88624459')]),
                        Column(children:[Text('การวิเคราะห์และออกแบบเชิงวัตถุ')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88646259')]),
                        Column(children:[Text('การประมวลผลภาษาธรรมชาติเบื้องต้น')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88634459')]),
                        Column(children:[Text('การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88624359')]),
                        Column(children:[Text('การเขียนโปรแกรมบนเว็บ')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88624559')]),
                        Column(children:[Text('การทดสอบซอฟต์แวร์')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('88634259')]),
                        Column(children:[Text('การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม')]),
                        Column(children:[Text('3')]),
                        Column(children:[Text('')]),
                      ]),
                      TableRow( children: [
                        Column(children:[Text('รวม',style: concludeStyle)]),
                        Column(children:[Text('')]),
                        Column(children:[Text('18',style: concludeStyle)]),
                        Column(children:[Text('0.00',style: concludeStyle)]),
                      ]),
                    ],
                  ),
                ),
              ),
            ]
          ),
        ),
      ),
    );
  }
  Widget dropDownYear() {
    return DropdownButton<String>(
      value: dropdownYear,
      elevation: 16,
      style: const TextStyle(color: Colors.black),
      underline: Container(
        height: 2,
        color: Colors.black26,
      ),
      onChanged: (String? value) {
        setState(() {
          dropdownYear = value!;
        });
      },
      items: yearList.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}