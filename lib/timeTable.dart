import 'package:flutter/material.dart';

class TimeTable extends StatefulWidget {
  const TimeTable({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _TimeTableState();
  }
}

List<String> yearList = <String>['2563', '2564', '2565', '2566'];

class _TimeTableState extends State<TimeTable> {
  static const TextStyle optionStyle =
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  static const TextStyle contentStyle =
      TextStyle(fontSize: 16, color: Colors.black);

  String dropdownYear = yearList.elementAt(2);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Card(
                  margin: EdgeInsets.all(5.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'ตารางเรียน/สอบ',
                        style: optionStyle,
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            '63160069 : นางสาวญาดา สุขวิพัฒน์ : คณะวิทยาการสารสนเทศ\n'
                            'หลักสูตร: 2115020: วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ : วิชาโท: 0: -\n'
                            'สถานภาพ: กำลังศึกษา\n'
                            'อ. ที่ปรึกษา: ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม\n',
                            style: contentStyle,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 0, 0, 20),
              child: Row(
                children: [
                  Text('ปีการศึกษา  ', style: contentStyle),
                  dropDownYear(),
                  Text(
                    '    ภาค    ',
                    style: contentStyle,
                  ),
                  semesterButton(),
                ],
              ),
            ),
            Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      width: 100,
                      height: 100,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.yellow),
                        ),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (ctx) => AlertDialog(
                              scrollable: true,
                              title: const Text("วันจันทร์"),
                              content: monday(),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(ctx).pop();
                                  },
                                  child: Container(
                                    color: Colors.grey.shade100,
                                    padding: const EdgeInsets.all(14),
                                    child: const Text("ปิด"),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        child: const Text("วันจันทร์"),
                      ),
                    ),
                    SizedBox(
                      width: 100,
                      height: 100,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.pink),
                        ),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (ctx) => AlertDialog(
                              scrollable: true,
                              title: const Text("วันอังคาร"),
                              content: tuesday(),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(ctx).pop();
                                  },
                                  child: Container(
                                    color: Colors.grey.shade100,
                                    padding: const EdgeInsets.all(14),
                                    child: const Text("ปิด"),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        child: const Text("วันอังคาร"),
                      ),
                    ),
                    SizedBox(
                      width: 100,
                      height: 100,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.green),
                        ),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (ctx) => AlertDialog(
                              scrollable: true,
                              title: const Text("วันพุธ"),
                              content: wednesday(),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(ctx).pop();
                                  },
                                  child: Container(
                                    color: Colors.grey.shade100,
                                    padding: const EdgeInsets.all(14),
                                    child: const Text("ปิด"),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        child: const Text("วันพุธ"),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20.0,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      width: 150,
                      height: 100,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.orange),
                        ),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (ctx) => AlertDialog(
                              title: const Text("วันพฤหัสบดี"),
                              content: const Text("-"),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(ctx).pop();
                                  },
                                  child: Container(
                                    color: Colors.grey.shade100,
                                    padding: const EdgeInsets.all(14),
                                    child: const Text("ปิด"),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        child: const Text("วันพฤหัสบดี"),
                      ),
                    ),
                    SizedBox(
                      width: 150,
                      height: 100,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.lightBlueAccent),
                        ),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (ctx) => AlertDialog(
                              scrollable: true,
                              title: const Text("วันศุกร์"),
                              content: friday(),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(ctx).pop();
                                  },
                                  child: Container(
                                    color: Colors.grey.shade100,
                                    padding: const EdgeInsets.all(14),
                                    child: const Text("ปิด"),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        child: const Text("วันศุกร์"),
                      ),
                    ),
                  ],
                ),

              ],
            ),
            examTable(),
          ],
        ),
      ),
    );
  }

  Widget dropDownYear() {
    return DropdownButton<String>(
      value: dropdownYear,
      elevation: 16,
      style: const TextStyle(color: Colors.black),
      underline: Container(
        height: 2,
        color: Colors.black26,
      ),
      onChanged: (String? value) {
        setState(() {
          dropdownYear = value!;
        });
      },
      items: yearList.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  Widget semesterButton() {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 40,
          height: 50,
          child: TextButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.grey),
            ),
            child: Text(
              '1',
              style: contentStyle,
            ),
            onPressed: () {},
          ),
        ),
        SizedBox(
          width: 40,
          height: 50,
          child: TextButton(
            autofocus: true,
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.grey),
            ),
            child: Text(
              '2',
              style: contentStyle,
            ),
            onPressed: () {},
          ),
        ),
        SizedBox(
          height: 50,
          child: TextButton(
            style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.grey)),
            child: Text(
              'ฤดูร้อน',
              style: contentStyle,
            ),
            onPressed: () {},
          ),
        ),
      ],
    );
  }

  Widget termButton() {
    return Row(
      children: <Widget>[
        SizedBox(
          width: 80,
          height: 50,
          child: TextButton(
            autofocus: true,
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.grey),
            ),
            child: Text(
              'กลางภาค',
              style: contentStyle,
            ),
            onPressed: () {},
          ),
        ),
        SizedBox(
          width: 80,
          height: 50,
          child: TextButton(
            autofocus: true,
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.grey),
            ),
            child: Text(
              'ปลายภาค',
              style: contentStyle,
            ),
            onPressed: () {},
          ),
        ),
      ],
    );
  }

  Widget examTable() {
    return Card(
      margin: EdgeInsets.all(10.0),
      shadowColor: Colors.grey[800],
      child: Container(
        child: Column(
          children: [
            Container(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: [
                        Text(
                          "ตารางสอบ",
                          style: TextStyle(fontSize: 20.0,
                          fontWeight: FontWeight.bold),
                        ),
                        termButton(),
                      ],
                    )
                  ],
              ),
            ),
            Table(
              defaultColumnWidth: FlexColumnWidth(20.0),
              children: [
                TableRow( children: [
                  Column(children:[Text('รหัสวิชา', style: contentStyle)]),
                  Column(children:[Text('วิชา', style: contentStyle)]),
                  Column(children:[Text('วัน-เวลา', style: contentStyle)]),
                  Column(children:[Text('สถานที่สอบ', style: contentStyle)]),
                ]),
                TableRow( children: [
                  Column(children:[Text('88624459')]),
                  Column(children:[Text('การวิเคราะห์และออกแบบเชิงวัตถุ')]),
                  Column(children:[Text('17 ม.ค.2566 17.00-20.00 น.')]),
                  Column(children:[Text('IF-11M280')]),
                ]),
                TableRow( children: [
                  Column(children:[Text('88646259')]),
                  Column(children:[Text('การประมวลผลภาษาธรรมชาติเบื้องต้น')]),
                  Column(children:[Text('18 ม.ค.2566 17.00-20.00 น.')]),
                  Column(children:[Text('IF-11M280')]),
                ]),
                TableRow( children: [
                  Column(children:[Text('88634459')]),
                  Column(children:[Text('การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1')]),
                  Column(children:[Text('20 ม.ค.2566 17.00-20.00 น.')]),
                  Column(children:[Text('IF-4C01')]),
                ]),
                TableRow( children: [
                  Column(children:[Text('88624359')]),
                  Column(children:[Text('การเขียนโปรแกรมบนเว็บ')]),
                  Column(children:[Text('16 ม.ค.2566 17.00-20.00 น.')]),
                  Column(children:[Text('IF-4C01')]),
                ]),
                TableRow( children: [
                  Column(children:[Text('88624559')]),
                  Column(children:[Text('การทดสอบซอฟต์แวร์')]),
                  Column(children:[Text('16 ม.ค.2566 09.00-12.00 น.')]),
                  Column(children:[Text('IF-3M210')]),
                ]),
                TableRow( children: [
                  Column(children:[Text('88634259')]),
                  Column(children:[Text('การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม')]),
                  Column(children:[Text('18 ม.ค.2566 13.00-16.00 น.')]),
                  Column(children:[Text('IF-3M2100')]),
                ]),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

//Day
Widget monday() {
  return Container(
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              width: 250,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('วิชา : การทดสอบซอฟต์แวร์'),
                      Text('เวลา : 10:00-12:00'),
                      Text('ห้อง : 4M210'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('วิชา : การวิเคราะห์และออกแบบเชิงวัตถุ'),
                      Text('เวลา : 13:00-15:00'),
                      Text('ห้อง : 3M210'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('วิชา : การเขียนโปรแกรมบนเว็บ'),
                      Text('เวลา : 17:00-19:00'),
                      Text('ห้อง : 3M210'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                ],
              ),
            ),
          ],
        )
      ],
    ),
  );
}

Widget tuesday() {
  return Container(
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              width: 250,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('วิชา : การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม'),
                      Text('เวลา : 10:00-12:00'),
                      Text('ห้อง : 4C01'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('วิชา : การวิเคราะห์และออกแบบเชิงวัตถุ'),
                      Text('เวลา : 13:00-15:00'),
                      Text('ห้อง : 4C01'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('วิชา : การทดสอบซอฟต์แวร์'),
                      Text('เวลา : 15:00-17:00'),
                      Text('ห้อง : 3C03'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                ],
              ),
            ),
          ],
        )
      ],
    ),
  );
}

Widget wednesday() {
  return Container(
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              width: 250,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'วิชา : การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1',
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        maxLines: 2,
                      ),
                      Text('เวลา : 10:00-12:00'),
                      Text('ห้อง : 4C01'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('วิชา : การประมวลผลภาษาธรรมชาติเบื้องต้น'),
                      Text('เวลา : 13:00-16:00'),
                      Text('ห้อง : 6T01'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('วิชา : การเขียนโปรแกรมบนเว็บ'),
                      Text('เวลา : 17:00-19:00'),
                      Text('ห้อง : 4C01'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                ],
              ),
            ),
          ],
        )
      ],
    ),
  );
}

Widget friday() {
  return Container(
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              width: 250,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('วิชา : การโปรแกรมสื่อผสมสำหรับหลายแพลตฟอร์ม'),
                      Text('เวลา : 13:00-15:00'),
                      Text('ห้อง : 4C01'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                          'วิชา : การพัฒนาโปรแกรมประยุกต์บนอุปกรณ์เคลื่อนที่ 1'),
                      Text('เวลา : 15:00-17:00'),
                      Text('ห้อง : 3C01'),
                      Text('กลุ่ม : 1'),
                    ],
                  ),
                ],
              ),
            ),
          ],
        )
      ],
    ),
  );
}
